$(document).ready(function(){
	$('input,textarea').focus(function(){
      $(this).data('placeholder',$(this).attr('placeholder'))
      $(this).attr('placeholder','');
    });
    $('input,textarea').blur(function(){
      $(this).attr('placeholder',$(this).data('placeholder'));
    });

     $('.put_text input').focus(function(){
     	$(this).css('color' , '#fff');
    	$(this).parent().css('background', '#1786c7')
    	$(this).parent().find('label').css('backgroundPosition', '0 1px')
    });
     $('.put_text input').focusout(function(){
     	$(this).css('color' , '#383838');
    	$(this).parent().css('background', 'none')
    	$(this).parent().find('label').css('backgroundPosition', '0 -16px')
    });

});